import React, {Component} from 'react'
import {View, Text, Image, Platform, StyleSheet} from 'react-native'

//ver o Navigation aqui: https://medium.com/@phylypo/react-native-navigation-with-switch-stack-and-tab-3506eafb16a0
import {createSwitchNavigator, createAppContainer} from 'react-navigation'

import Loading from './Components/Loading'
import SignUp from './Components/SignUp'
import Login from './Components/Login'
import Main from './Components/Main'

const AppNavigator = createSwitchNavigator({
    loading: Loading,
    signUp: SignUp,
    login: Login,
    main: Main
  },{
    initialRouteName: 'loading'
  }
)

const AppContainer = createAppContainer(AppNavigator)

export default class App extends Component{
  render(){
    return(
      <AppContainer />
    )
  }
}